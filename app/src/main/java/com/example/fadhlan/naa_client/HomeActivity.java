package com.example.fadhlan.naa_client;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import com.example.fadhlan.naa_client.helpers.AdapterMessage;
import com.example.fadhlan.naa_client.helpers.HelperAPI;
import com.example.fadhlan.naa_client.helpers.HelperDB;
import com.example.fadhlan.naa_client.helpers.MyFirebaseInstanceIDService;
import com.example.fadhlan.naa_client.models.Message;
import com.example.fadhlan.naa_client.models.Device;
import com.google.gson.Gson;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {
    private TextView tv_user_id;
    private TextView tv_user_token;
    private RecyclerView rv_messages;
    private AdapterMessage adapterInboxMessage;

    private HelperDB helperDB;
    private HelperAPI helperAPI;
    private Device user = new Device();
    private List<Message> messages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();

        helperDB = new HelperDB(this);
        helperAPI = new HelperAPI();

        MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
        myFirebaseInstanceIDService.onTokenRefresh();

        String myToken = myFirebaseInstanceIDService.getRefreshedToken();

        user = helperDB.getUserbyToken(myToken);

        if(myToken != null && myToken != "" && user.getId() == 0){
            pushNewToken(myToken);
        }

        tv_user_id.setText(user.getId().toString());
        tv_user_token.setText(user.getToken().toString());
        getInboxMessage();
    }

    public void init(){
        tv_user_id = (TextView) findViewById(R.id.tv_user_id);
        tv_user_token = (TextView) findViewById(R.id.tv_user_token);
        rv_messages = (RecyclerView) findViewById(R.id.rv_message);
        rv_messages.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
    }

    void pushNewToken(final String token){
        JSONObject params = new JSONObject();
        user.setToken(token);
        try{
            params.put("token", user.getToken());

            helperAPI.post(this, "/device/", new StringEntity(params.toString()),  new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try{
                        user.setId(Integer.parseInt(response.get("id").toString()));
                        helperDB.setUser(user);
                    }
                    catch(JSONException error){
                        System.out.println("error"+error);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    System.out.println("error"+errorResponse);
                }

            });
        }
        catch (UnsupportedEncodingException error){
            System.out.println(error);
        }
        catch (JSONException error){
            System.out.println(error);
        }
    }

    public void showInRV(List<Message> messages) {
        adapterInboxMessage = new AdapterMessage(messages);
        rv_messages.setAdapter(adapterInboxMessage);
    }

    public void getInboxMessage(){
        helperAPI.get("/device/"+user.getId()+"/receivedMessage", null, null,  new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                try{
                    for(int i=0; i<response.length(); i++){
                        JSONObject object = new JSONObject();
                        object = (JSONObject) response.get(i);

                        Message message = new Message();
                        Gson gson = new Gson();
                        message = gson.fromJson(object.toString(), Message.class);

                        messages.add(message);
                    }

                    showInRV(messages);
                }
                catch(JSONException error){
                    System.out.println("error"+error);
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                System.out.println("error"+errorResponse);
            }
        });
    }
}
