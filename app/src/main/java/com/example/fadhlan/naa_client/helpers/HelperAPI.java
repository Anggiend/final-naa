package com.example.fadhlan.naa_client.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;

public class HelperAPI {
    private static AsyncHttpClient client = new AsyncHttpClient();
    //Base URL untuk dev
    private static final String BASE_URL = "http://192.168.5.27:8080";


    static SharedPreferences sharedPreferences;

    public static void get(String url, RequestParams params, String header, AsyncHttpResponseHandler responseHandler) {
        client.setEnableRedirects(true, true, true);
        client.setTimeout(100000);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, StringEntity entity, AsyncHttpResponseHandler responseHandler){
        client.setEnableRedirects(true, true, true);
        client.setTimeout(100000);
        client.post(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
    }

    public static void put(Context context, String url, StringEntity  entity, AsyncHttpResponseHandler responseHandler){
        client.setEnableRedirects(true, true, true);
        client.setTimeout(100000);
        client.put(context, getAbsoluteUrl(url), entity, "application/json", responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        System.out.print("BASE CLIENT : "+ BASE_URL + relativeUrl);
        return BASE_URL + relativeUrl;
    }
}
