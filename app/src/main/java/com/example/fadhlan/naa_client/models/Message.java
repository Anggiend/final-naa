package com.example.fadhlan.naa_client.models;

public class Message {
    private String messageId;
    private Device sender;
    private Device receiver;
    private String body;
    private Long sentDate;
    private Long receiveDate;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public Device getSender() {
        return sender;
    }

    public void setSender(Device sender) {
        this.sender = sender;
    }

    public Device getReceiver() {
        return receiver;
    }

    public void setReceiver(Device receiver) {
        this.receiver = receiver;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Long getSentDate() {
        return sentDate;
    }

    public void setSentDate(Long sentDate) {
        this.sentDate = sentDate;
    }

    public Long getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(Long receiveDate) {
        this.receiveDate = receiveDate;
    }
}
